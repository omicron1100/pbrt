#include <vector>

class Integrator {
  public:
    virtual void Render() = 0;
    pstd::optional<ShapeIntersection> Integrator::Intersect(const Ray &ray, Float tMax) const {
        if (aggregate)
            return aggregate.Intersect(ray, tMax);
        else
            return {};
    }
    bool Integrator::IntersectP(const Ray &ray, Float tMax) const {
        if (aggregate)
            return aggregate.IntersectP(ray, tMax);
        else
            return false;
    }

    Primitive aggregate;
    std::vector<Light> lights;
    std::vector<Light> infiniteLights;

  protected:
    Integrator(Primitive aggregate, std::vector<Light> lights) : aggregate(aggregate), lights(lights) {
        Bounds3f sceneBounds = aggregate ? aggregate.Bounds() : Bounds3f();
        for (auto &light : lights) {
            light.Preprocess(sceneBounds);
            if (light.Type() == LightType::Infinite)
                infiniteLights.push_back(light);
        }
    }
};

class ImageTileIntegrator : public Integrator {
  public:
    ImageTileIntegrator(Camera camera, Sampler sampler, Primitive aggregate, std::vector<Light> lights)
        : Integrator(aggregate, lights), camera(camera), samplerPrototype(sampler) {

    }
    virtual void EvaluatePixelSample(Point2i pPixel, int sampleIndex, Sampler sampler, ScratchBuffer &scratchBuffer) = 0;

  protected:
    Camera camera;
    Sampler samplerPrototype;
};

class RayIntegrator : public Integrator {
  public:
    RayIntegrator(Camera camera, Sampler sampler, Primitive aggregate, std::vector<Light> lights)
        : ImageTileIntegrator(camera, sampler, aggregate, lights) {
        
    }
    virtual SampledSpectrum Li(RayDifferential ray, SampledWavelengths &lambda, Sampler sampler, ScratchBuffer &scratchBuffer, VisibleSurface *visibleSurface) const = 0;
};

class RandomWalkIntegrator : public RayIntegrator {
  public:
    SampledSpectrum Li(RayDifferential ray, SampledWavelengths &lambda, Sampler sampler, ScratchBuffer &scratchBuffer, VisibleSurface *visibleSurface) const {
      return LiRandomWalk(ray, lambda, sampler, scratchBuffer, 0);
    }
  private:
    SampledSpectrum LiRandomWalk(RayDifferential ray, SampledWavelengths &lambda, Sampler sampler, ScratchBuffer &scratchBuffer, int depth) const {
      
      //intersect ray with scene and return if no intersection
      pstd::optional<ShapeIntersection> si = Intersect(ray);
      if (!si) {
        //return emitted light from infinite light sources
        SampledSpectrum Le(0.f);
        for (Light light : infiniteLights)
          Le += light.Le(ray, lambda);
          return Le;
      }
      SurfaceInteraction &isect = si->intr;

      //get emitted radiance at surface intersection
      Vector3f wo = -ray.d;
      SampledSpectrum Le = isect.Le(wo, lambda);

      //terminate random walk if maximum depth is reached
      if (depth == maxDepth)
        return Le;
      
      //compute BSDF at random walk intersection point
      BSDF bsdf = isect.GetBSDF(ray, lambda, camera, scratchBuffer, sampler);

      //randomly sample direction leaving surface for random walk
      Point2f u = sampler.Get2D();
      Vector3f wp = SampleUniformSphere(u);

      //evaluate BSDF at surface for sampled direction
      SampledSpectrum fcos = bsdf.f(wo, wp) * AbsDot(wp, isect.shading.n);
      if (!fcos)
        return Le;

      //recursively trace ray to estimate incident radiance at surface
      ray = isect.SpawnRay(wp);
      return Le + fcos * LiRandomWalk(ray, lambda, sampler, scratchBuffer, depth + 1) / (1 / (4 * Pi));
    }
}