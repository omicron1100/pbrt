

void ImageTileIntegrator::Render() {
    //declare common variables for rendering images in tiles
    ThreadLocal<ScratchBuffer> scratchBuffers([]() { return ScratchBuffer(); });
    ThreadLocal<Sampler> samplers([this]() { return samplerPrototype.Clone(); });
    Bounds2i pixelBounds = camera.GetFilm().PixelBounds();
    int spp = samplerPrototype.SamplesPerPixel();
    ProgressReporter progress(int64_t(spp) * pixelBounds.Area(), "Rendering", Options->quiet);
    int waveStart = 0, waveEnd = 1, nextWaveSize = 1;

    //render images in waves
    while (waveStart < spp) {
        //render current wave's image tiles in parallel
        ParallelFor2D(pixelBounds, [&](Bounds2i tileBounds) {
            //render image tile given by tileBounds
            ScratchBuffer &scratchBuffer = scratchBuffers.Get();
            Sampler &sampler = samplers.Get();
            for (Point2i pPixel : tileBounds) {
                //render samples in pixel pPixel
                for (int sampleIndex = waveStart; sampleIndex < waveEnd; ++sampleIndex) {
                    sampler.StartPixelSample(pPixel, sampleIndex);
                    EvaluatePixelSample(pPixel, sampleIndex, sampler, scratchBuffer);
                    scratchBuffer.Reset();
                }
            }
            progress.Update((waveEnd - waveStart) * tileBounds.Area());
        });

        //update start and end wave
        waveStart = waveEnd;
        waveEnd = std::min(spp, waveEnd + nextWaveSize);
        nextWaveSize = std::min(2 * nextWaveSize, 64);
    }
}

void RayIntegrator::EvaluatePixelSample(Point2i pPixel, int sampleIndex, Sampler sampler, ScratchBuffer &scratchBuffer) {
    //sample wavelengths for ray
    Float lu = sampler.Get1D();
    SampledWavelengths lambda = camera.GetFilm().SampleWavelengths(lu);

    //initialize CameraSample for current sample
    Filter filter = camera.GetFilm().GetFilter();
    CameraSample cameraSample = GetCameraSample(sampler, pPixel, filter);

    //generate camera ray for current sample
    pstd::optional<CameraRayDifferential> cameraRay = camera.GenerateRayDifferential(cameraSample, lambda);

    //trace cameraRay if valid
    SampledSpectrum L(0.);
    VisibleSurface visibleSurface;
    if (cameraRay) {
        //scale camera ray differentials based on image sampling rate
        Float rayDiffScale = std::max<Float>(.125f, 1 / std::sqrt((Float)sampler.SamplesPerPixel()));
        cameraRay->ray.ScaleDifferentials(rayDiffScale);

        //evaluate radiance along camera ray
        bool initializeVisibleSurface = camera.GetFilm().UsesVisibleSurface();
        L = cameraRay->weight * Li(cameraRay->ray, lambda, sampler, scratchBuffer, initializeVisibleSurface ? &visibleSurface : nullptr);

        //issue warning if unexpected radiance value returns
    }

    //add camera ray's contribution to image
    camera.GetFilm().AddSample(pPixel, L, lambda, &visibleSurface, cameraSample.filterWeight);
    
}