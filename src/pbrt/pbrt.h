#ifndef PBRT_PBRT_H
#define PBRT_PBRT_H

#include <stdint.h>
#include <cstddef>

#ifndef PBRT_FLOAT_AS_DOUBLE
    using Float = double;
#else
    using Float = float;
#endif

using Allocator = pstd::pmr::polymorphic_allocator<std::byte>;