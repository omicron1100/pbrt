//reimplementation of std functions for interchangeable use on the CPU and GPU
#ifndef PBRT_UTIL_PSTD_H
#define PBRT_UTIL_PSTD_H

namespace pstd {

template <typename T>
PBRT_CPU_GPU inline void swap(T &a, T &b) {
    T tmp = std::move(a);
    a = std::move(b);
    b = std::move(tmp);
}

template <class To, class From>
PBRT_CPU_GPU typename std::enable_if_t<sizeof(To) == sizeof(From) &&
                                       std::is_trivially_copyable_v<From> &&
                                       std::is_trivially_copyable_v<To>, To>
bit_cast(const From &src) noexcept {
    static_assert(std::is_trivially_constructible_v<To>,
                  "This implementation requires the destination type to be trivially constructible");
    To dst;
    std::memcpy(&dst, &src, sizeof(To));
    return dst;
}

template<typename T>
class optional {
  public:
    using value_type = T;

    optional() = default;
    optional(const T &v) : set(true) { new (ptr()) T(v); }
    optional(T &&v) : set(true) { new (ptr()) T(std::move(v)); }
    optional(const optional &v) : set(v.has_value()) {
        if (v.has_value())
            new (ptr()) T(v.value());
    }
    optional(optional &&v) : set(v.has_value()) {
        if (v.has_value()) {
            new (ptr()) T(std::move(v.value()));
            v.reset();
        }
    }
}