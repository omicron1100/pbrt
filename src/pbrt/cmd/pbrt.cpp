#include <vector>
#include <string>

static void usage(const std::string &msg = {}) {

}

int main(int argc, char *argv[]) {
	//convert command-line arguments to vector of strings
	std::vector<std::string> args = GetCommandLineArguments(argv);

	//declare variables for parsed command line
	PBRTOptions options;
	std::vector<std::string> filenames;

	//process command-line arguments
	for(auto it = args.begin(); it != args.end(); ++it) {
		if((*it)[0] != '-') {
			filenames.push_back(*it);
			continue;
		}

		auto onError = [](const std::string &err) {
			usage(err);
			exit(1);
		};
		
		std::string cropWindow, pixelBounds, pixel, pixelMaterial;
		if(ParseArg(&it, args.end(), "cropwindow", &cropWindow, onError)) {
			std::vector<Float> c = SplitStringToFloats(cropWindow, ',');
			if (c.size() != 4) {
				usage("Didn't find two values after --pixel");
				return 1;
			}
		} else if (*it == "--help" || *it == "-help" || *it == "-h") {
		} else {
			usage(StringPrintf("argument \"%s\" unknown", *it));
			return 1;
		}
	}

	//initialize pbrt
	InitPBRT(options);
	
	//parse provided scene description files
	BasicScene scene;
	BasicSceneBuilder builder(&scene);
	ParseFiles(&builder, filenames);

	//render scene
	if (Options->useGPU || Options->wavefront) {
		RenderWavefront(scene);
	} else {
		RenderCPU(scene);
	}

	//clean up after rendering scene
	CleanupPBRT();
}


